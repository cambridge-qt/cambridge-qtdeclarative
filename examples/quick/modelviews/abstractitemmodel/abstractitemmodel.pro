TEMPLATE = app
TARGET = abstractitemmodel
DEPENDPATH += .
INCLUDEPATH += .
QT += qml quick

HEADERS = model.h
SOURCES = main.cpp \
          model.cpp
RESOURCES += abstractitemmodel.qrc
