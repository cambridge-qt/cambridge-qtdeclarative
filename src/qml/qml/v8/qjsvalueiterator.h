/****************************************************************************
**
** Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/
**
** This file is part of the QtQml module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this
** file. Please review the following information to ensure the GNU Lesser
** General Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QSCRIPTVALUEITERATOR_H
#define QSCRIPTVALUEITERATOR_H

#include <QtQml/qtqmlglobal.h>
#include <QtQml/qjsvalue.h>
#include <QtCore/qscopedpointer.h>

QT_BEGIN_HEADER

QT_BEGIN_NAMESPACE


class QString;

class QJSValueIteratorPrivate;
class Q_QML_EXPORT QJSValueIterator
{
public:
    QJSValueIterator(const QJSValue &value);
    ~QJSValueIterator();

    bool hasNext() const;
    bool next();

    QString name() const;

    QJSValue value() const;
    QJSValueIterator& operator=(QJSValue &value);

private:
    QScopedPointer<QJSValueIteratorPrivate> d_ptr;

    Q_DECLARE_PRIVATE(QJSValueIterator)
    Q_DISABLE_COPY(QJSValueIterator)
};

QT_END_NAMESPACE

QT_END_HEADER

#endif // QSCRIPTVALUEITERATOR_H
