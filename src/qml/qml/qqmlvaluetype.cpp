/****************************************************************************
**
** Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/
**
** This file is part of the QtQml module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this
** file. Please review the following information to ensure the GNU Lesser
** General Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qqmlvaluetype_p.h"
#include "qqmlmetatype_p.h"

#include <private/qqmlglobal_p.h>
#include <QtCore/qdebug.h>

QT_BEGIN_NAMESPACE

QQmlValueTypeFactory::QQmlValueTypeFactory()
{
    for (unsigned int ii = 0; ii < QVariant::UserType; ++ii)
        valueTypes[ii] = 0;
}

QQmlValueTypeFactory::~QQmlValueTypeFactory()
{
    for (unsigned int ii = 0; ii < QVariant::UserType; ++ii)
        delete valueTypes[ii];
}

bool QQmlValueTypeFactory::isValueType(int idx)
{
    if ((uint)idx < QVariant::UserType
            && idx != QVariant::StringList
            && idx != QMetaType::QObjectStar
            && idx != QMetaType::QWidgetStar
            && idx != QMetaType::VoidStar
            && idx != QMetaType::QVariant) {
        return true;
    }
    return false;
}

void QQmlValueTypeFactory::registerBaseTypes(const char *uri, int versionMajor, int versionMinor)
{
    qmlRegisterValueTypeEnums<QQmlEasingValueType>(uri, versionMajor, versionMinor, "Easing");
}

void QQmlValueTypeFactory::registerValueTypes()
{
    registerBaseTypes("QtQuick", 2, 0);
}

QQmlValueType *QQmlValueTypeFactory::valueType(int t)
{
    QQmlValueType *rv = 0;

    switch (t) {
    case QVariant::Point:
        rv = new QQmlPointValueType;
        break;
    case QVariant::PointF:
        rv = new QQmlPointFValueType;
        break;
    case QVariant::Size:
        rv = new QQmlSizeValueType;
        break;
    case QVariant::SizeF:
        rv = new QQmlSizeFValueType;
        break;
    case QVariant::Rect:
        rv = new QQmlRectValueType;
        break;
    case QVariant::RectF:
        rv = new QQmlRectFValueType;
        break;
    case QVariant::EasingCurve:
        rv = new QQmlEasingValueType;
        break;
    default:
        rv = QQml_valueTypeProvider()->createValueType(t);
        break;
    }

    Q_ASSERT(!rv || rv->metaObject()->propertyCount() < 32);
    return rv;
}

QQmlValueType::QQmlValueType(QObject *parent)
: QObject(parent)
{
}


QQmlPointFValueType::QQmlPointFValueType(QObject *parent)
    : QQmlValueTypeBase<QPointF>(parent)
{
}

QString QQmlPointFValueType::toString() const
{
    return QString(QLatin1String("QPointF(%1, %2)")).arg(v.x()).arg(v.y());
}

qreal QQmlPointFValueType::x() const
{
    return v.x();
}

qreal QQmlPointFValueType::y() const
{
    return v.y();
}

void QQmlPointFValueType::setX(qreal x)
{
    v.setX(x);
}

void QQmlPointFValueType::setY(qreal y)
{
    v.setY(y);
}


QQmlPointValueType::QQmlPointValueType(QObject *parent)
    : QQmlValueTypeBase<QPoint>(parent)
{
}

QString QQmlPointValueType::toString() const
{
    return QString(QLatin1String("QPoint(%1, %2)")).arg(v.x()).arg(v.y());
}

int QQmlPointValueType::x() const
{
    return v.x();
}

int QQmlPointValueType::y() const
{
    return v.y();
}

void QQmlPointValueType::setX(int x)
{
    v.setX(x);
}

void QQmlPointValueType::setY(int y)
{
    v.setY(y);
}


QQmlSizeFValueType::QQmlSizeFValueType(QObject *parent)
    : QQmlValueTypeBase<QSizeF>(parent)
{
}

QString QQmlSizeFValueType::toString() const
{
    return QString(QLatin1String("QSizeF(%1, %2)")).arg(v.width()).arg(v.height());
}

qreal QQmlSizeFValueType::width() const
{
    return v.width();
}

qreal QQmlSizeFValueType::height() const
{
    return v.height();
}

void QQmlSizeFValueType::setWidth(qreal w)
{
    v.setWidth(w);
}

void QQmlSizeFValueType::setHeight(qreal h)
{
    v.setHeight(h);
}


QQmlSizeValueType::QQmlSizeValueType(QObject *parent)
    : QQmlValueTypeBase<QSize>(parent)
{
}

QString QQmlSizeValueType::toString() const
{
    return QString(QLatin1String("QSize(%1, %2)")).arg(v.width()).arg(v.height());
}

int QQmlSizeValueType::width() const
{
    return v.width();
}

int QQmlSizeValueType::height() const
{
    return v.height();
}

void QQmlSizeValueType::setWidth(int w)
{
    v.setWidth(w);
}

void QQmlSizeValueType::setHeight(int h)
{
    v.setHeight(h);
}


QQmlRectFValueType::QQmlRectFValueType(QObject *parent)
    : QQmlValueTypeBase<QRectF>(parent)
{
}

QString QQmlRectFValueType::toString() const
{
    return QString(QLatin1String("QRectF(%1, %2, %3, %4)")).arg(v.x()).arg(v.y()).arg(v.width()).arg(v.height());
}

qreal QQmlRectFValueType::x() const
{
    return v.x();
}

qreal QQmlRectFValueType::y() const
{
    return v.y();
}

void QQmlRectFValueType::setX(qreal x)
{
    v.moveLeft(x);
}

void QQmlRectFValueType::setY(qreal y)
{
    v.moveTop(y);
}

qreal QQmlRectFValueType::width() const
{
    return v.width();
}

qreal QQmlRectFValueType::height() const
{
    return v.height();
}

void QQmlRectFValueType::setWidth(qreal w)
{
    v.setWidth(w);
}

void QQmlRectFValueType::setHeight(qreal h)
{
    v.setHeight(h);
}


QQmlRectValueType::QQmlRectValueType(QObject *parent)
    : QQmlValueTypeBase<QRect>(parent)
{
}

QString QQmlRectValueType::toString() const
{
    return QString(QLatin1String("QRect(%1, %2, %3, %4)")).arg(v.x()).arg(v.y()).arg(v.width()).arg(v.height());
}

int QQmlRectValueType::x() const
{
    return v.x();
}

int QQmlRectValueType::y() const
{
    return v.y();
}

void QQmlRectValueType::setX(int x)
{
    v.moveLeft(x);
}

void QQmlRectValueType::setY(int y)
{
    v.moveTop(y);
}

int QQmlRectValueType::width() const
{
    return v.width();
}

int QQmlRectValueType::height() const
{
    return v.height();
}

void QQmlRectValueType::setWidth(int w)
{
    v.setWidth(w);
}

void QQmlRectValueType::setHeight(int h)
{
    v.setHeight(h);
}


QQmlEasingValueType::QQmlEasingValueType(QObject *parent)
    : QQmlValueTypeBase<QEasingCurve>(parent)
{
}

QString QQmlEasingValueType::toString() const
{
    return QString(QLatin1String("QEasingCurve(%1, %2, %3, %4)")).arg(v.type()).arg(v.amplitude()).arg(v.overshoot()).arg(v.period());
}

QQmlEasingValueType::Type QQmlEasingValueType::type() const
{
    return (QQmlEasingValueType::Type)v.type();
}

qreal QQmlEasingValueType::amplitude() const
{
    return v.amplitude();
}

qreal QQmlEasingValueType::overshoot() const
{
    return v.overshoot();
}

qreal QQmlEasingValueType::period() const
{
    return v.period();
}

void QQmlEasingValueType::setType(QQmlEasingValueType::Type type)
{
    v.setType((QEasingCurve::Type)type);
}

void QQmlEasingValueType::setAmplitude(qreal amplitude)
{
    v.setAmplitude(amplitude);
}

void QQmlEasingValueType::setOvershoot(qreal overshoot)
{
    v.setOvershoot(overshoot);
}

void QQmlEasingValueType::setPeriod(qreal period)
{
    v.setPeriod(period);
}

void QQmlEasingValueType::setBezierCurve(const QVariantList &customCurveVariant)
{
    if (customCurveVariant.isEmpty())
        return;

    QVariantList variantList = customCurveVariant;
    if ((variantList.count() % 6) == 0) {
        bool allRealsOk = true;
        QList<qreal> reals;
        for (int i = 0; i < variantList.count(); i++) {
            bool ok;
            const qreal real = variantList.at(i).toReal(&ok);
            reals.append(real);
            if (!ok)
                allRealsOk = false;
        }
        if (allRealsOk) {
            QEasingCurve newEasingCurve(QEasingCurve::BezierSpline);
            for (int i = 0; i < reals.count() / 6; i++) {
                const qreal c1x = reals.at(i * 6);
                const qreal c1y = reals.at(i * 6 + 1);
                const qreal c2x = reals.at(i * 6 + 2);
                const qreal c2y = reals.at(i * 6 + 3);
                const qreal c3x = reals.at(i * 6 + 4);
                const qreal c3y = reals.at(i * 6 + 5);

                const QPointF c1(c1x, c1y);
                const QPointF c2(c2x, c2y);
                const QPointF c3(c3x, c3y);

                newEasingCurve.addCubicBezierSegment(c1, c2, c3);
                v = newEasingCurve;
            }
        }
    }
}

QVariantList QQmlEasingValueType::bezierCurve() const
{
    QVariantList rv;
    QVector<QPointF> points = v.toCubicSpline();
    for (int ii = 0; ii < points.count(); ++ii)
        rv << QVariant(points.at(ii).x()) << QVariant(points.at(ii).y());
    return rv;
}

QT_END_NAMESPACE
